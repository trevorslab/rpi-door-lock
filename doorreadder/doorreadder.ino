#include <RFID.h>
#include <SPI.h>
#define SS_PIN 10
#define RST_PIN 9

RFID rfid(SS_PIN,RST_PIN);
void setup(){

    Serial.begin(9600);
    SPI.begin();
    rfid.init();
   
}

void loop(){
  if(rfid.isCard()){
    if(rfid.readCardSerial()){
      String str1(rfid.serNum[0]);
      String str2(rfid.serNum[1]);
      String str3(rfid.serNum[2]);
      String str4(rfid.serNum[3]);
      String str5(rfid.serNum[4]);
      String str = str1 + str2 + str3 + str4 + str5;
      Serial.println(str);
      delay(1000);
    }
  }
}

