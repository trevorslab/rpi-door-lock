import thread
from subprocess import call
import threading
import sys
import time
import RPi.GPIO as GPIO
import serial
import requests
unlockTime = 2
GPIO.setup(7, GPIO.OUT)
GPIO.setup(11, GPIO.OUT)
GPIO.output(7, GPIO.HIGH)
GPIO.output(11, GPIO.HIGH)
GPIO.setup(13, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
def open():
    GPIO.output(7, GPIO.LOW)
    GPIO.output(11, GPIO.LOW)
    time.sleep(unlockTime)
    GPIO.output(7, GPIO.HIGH)
    GPIO.output(11, GPIO.HIGH)

def exitThread():
    while True: # Run forever
        time.sleep(0.5)
        if GPIO.input(13) == GPIO.HIGH:
            time.sleep(0.1)
            if GPIO.input(13) == GPIO.HIGH:
                print("Exit Requested")
                open()
                time.sleep(unlockTime)
thread.start_new_thread(exitThread,())
ser = serial.Serial('/dev/serial0', 9600)
while True:
	num = ser.readline()
	r = requests.get('http://example.com/bacs/api/acu.php?acuID=2&action=auth&cardID=' + num)
        print(r)
        if r.text == "authorized":
                open()
	print num
ser.close()
